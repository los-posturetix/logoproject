const path = require('path'),
  fs = require('fs'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  MiniCssExtractPlugin = require('mini-css-extract-plugin'),
  { CleanWebpackPlugin } = require('clean-webpack-plugin');


//** CONFIG VARIABLES */

const pages_dir = path.resolve(__dirname, 'src/pages/');



var htmlWebpackPluginsPages = ((dir) => {

  return fs.readdirSync(dir).map((folder) =>
    new HtmlWebpackPlugin({
      template: `${path.resolve(dir, folder + "/" + folder)}.pug`,
      inject: true,
      chunks: ['vendors', `${folder}`],
      filename: `${folder.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase()}.html`
    }));



})(pages_dir);





module.exports = {
  context: __dirname,
  entry: {
    index: ['./src/pages/index/index.es6'],
    contacto: ['./src/pages/contacto/contacto.es6'],
    servicios: ['./src/pages/servicios/servicios.es6'],
    sobreNosotros: ['./src/pages/sobreNosotros/sobreNosotros.es6'],
    vendors: ['./src/vendors/vendors.es6']
  },
  output: {
    filename: '[name].js'
  },
  resolve: {
    alias: {
      Stylesheet$: path.resolve(__dirname, 'src/stylesheet/main.scss'),
      images:path.resolve(__dirname,'src/assets/images/')
    }
  },

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: false,
    port: 9000,
    writeToDisk: true,
    staticOptions: {
      dotfiles: 'ignore',
      etag: false,
      extensions: ['htm', 'html'],
      index: false,
      maxAge: '1d',
      redirect: false,
      watchContentBase: true
    }
  },

  module: {
    rules: [
      {
        test: /(\.js|.es6)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /.pug$/,
        exclude: /node_modules/,
        use: {
          loader: 'pug-loader',
          query: {}
        }
      },
      {
        test: /\.scss$/,
        use: [
          // fallback to style-loader in development
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|svg|jp(e*)g)$/,
        use: [{
          loader: 'url-loader',
          options: {
            name: 'images/[hash]-[name].[ext]'
          }
        }]
      },
     
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin()
  ].concat(htmlWebpackPluginsPages)
}